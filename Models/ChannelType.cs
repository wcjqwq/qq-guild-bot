﻿namespace QqChannelRobotSdk.Models;

public enum ChannelType
{
    Unknown,
    Text = 0,
    Reserve,
    Voice,
    Reserve1,
    Group,
    Live,
    Application,
    Forum
}