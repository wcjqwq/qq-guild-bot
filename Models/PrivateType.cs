﻿namespace QqChannelRobotSdk.Models;

public enum PrivateType
{
    Unknown = -1,
    Everyone,
    Admins,
    AllowedMembers
}