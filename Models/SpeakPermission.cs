﻿namespace QqChannelRobotSdk.Models;

public enum SpeakPermission
{
    Unknown = -1,
    Invalid,
    Everyone,
    AllowedMembers
}