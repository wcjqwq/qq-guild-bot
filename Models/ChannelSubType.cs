﻿namespace QqChannelRobotSdk.Models;

public enum ChannelSubType
{
    Unknown = -1,
    Chat,
    Announcement,
    Strategy,
    GangUp
}