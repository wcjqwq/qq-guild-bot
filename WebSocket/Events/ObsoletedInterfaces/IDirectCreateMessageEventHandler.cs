﻿using QqChannelRobotSdk.WebSocket.Events.EventArgs;

namespace QqChannelRobotSdk.WebSocket.Events;

public interface IDirectCreateMessageEventHandler : IEventHandler<DirectCreateMessageEventArgs>
{
}