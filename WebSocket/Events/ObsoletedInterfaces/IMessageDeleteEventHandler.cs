﻿using QqChannelRobotSdk.WebSocket.Events.EventArgs;

namespace QqChannelRobotSdk.WebSocket.Events;

public interface IMessageDeleteEventHandler : IEventHandler<MessageDeleteEventArgs>
{
}