﻿namespace QqChannelRobotSdk.Authenticate;

public enum BotAuthenticateType
{
    BotToken,
    BearerToken
}