﻿namespace QqChannelRobotSdk.Messages.Keyboard;

public enum MessageKeyboardPermissionType
{
    AllowedUsers,
    Admins,
    Everyone,
    AllowedRoles
}