﻿namespace QqChannelRobotSdk.Messages.Keyboard;

public enum ActionType
{
    HyperLink,
    Callback,
    AtRobot
}