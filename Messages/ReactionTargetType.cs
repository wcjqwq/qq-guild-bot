﻿namespace QqChannelRobotSdk.Messages;

public enum ReactionTargetType
{
    None = -1,
    Message,
    Post,
    Comment,
    Reply
}