﻿namespace QqChannelRobotSdk.Audio;

public enum AudioStatus
{
    Start,
    Pause,
    Resume,
    Stop
}